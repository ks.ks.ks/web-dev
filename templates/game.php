<!DOCTYPE html>
<html lang="en">
<head>
<!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();
        for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
            k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
 
        ym(93760794, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
 <noscript><div><img src="https://mc.yandex.ru/watch/93760794" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
 <!-- /Yandex.Metrika counter -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/single_game.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,100&display=swap" rel="stylesheet">

    <title>Games Catalog</title>
</head>
<body>
<header class="header">
        <div class = logo>
            <h2 class="header__title">Games Catalog</h2>
            <img class = "img_logo" width="125" height="125" alt="Gaming" src="../images/LOGO_PNG.png">
        </div>
        <ul class="header__navigation">
            <li class="header__element">
                <h3><a class="nav__element" href="../index.php">home</a></h3>
            </li>
            <li class="header__element">
                <div class="dropdown">
                    <h3 class="nav__element">categories🔽</h3>
                    <div class="dropdown-content">
                      <a href="catalog_genres.php">Genres</a>
                      <a href="catalog_themes.php">Themes</a>
                      <a href="catalog_number_of_players.php">Number of players</a>
                    </div>
                  </div>
            </li>
            <li class="header__element">
                <h3 class="nav__element">about us</h3>
            </li>
            <li class="header__element">
                <h3 class="nav__element">contact</h3>
            </li>
        </ul>    
    </header>
<main class="main">
        <div class="game__info">
            <div class = title> 
                <?php $var2 = $_GET['id_game'];
                 
                        require 'game_info_pages.php';
                        foreach($result2 as $row2){
                            ?><?php echo $row2['title']; echo "<br>"; ?><?php
                        } 
                ?>
            </div> 
            <div class =dev> Разработчик:   
                <?php $var2 = $_GET['id_game']; 
                        require 'game_info_pages.php'; 
                        foreach($result2 as $row2){
                            ?> <?php echo $row2['developer']; echo "<br>"; ?><?php
                        }   
                ?>
            </div>
            <div class =date_release> Дата выхода:   
                <?php $var2 = $_GET['id_game']; 
                        require 'game_info_pages.php'; 
                        foreach($result2 as $row2){
                            ?> <?php echo $row2['release_date']; echo "<br>"; ?><?php
                        }   
                ?>
            
            </div>
            <div class = descrip> Описание: </br></br>
                <?php $var2 = $_GET['id_game']; 
                        require 'game_info_pages.php'; 
                        foreach($result2 as $row2){
                            echo $row2['description']; echo "<br>";echo "<br>";
                        }   
                ?>
            </div>

            <div class = image>  </br>
                <?php 
                    $var2 = $_GET['id_game']; 
                    require 'game_info_pages.php'; 
                    while($row2 = mysqli_fetch_assoc($result2))
                        {
                            echo ("<img src=" . $row2['image'] . " width = '500px', height = '350px'>");
                        }
                ?>
            </div> 
        </div>
    </main>
    <footer class="footer">
        <div class="contacts">
            <div class="footer__text">
                <p class="logo__text">
                    Games catalog
                </p>
            </div>
            <ul class="contacts__navigation">
                <li class="contact__element">
                    <img class="icon__facebook" width="23" height="22" alt="facebook" src="../images/facebook.svg">
                </li>
                <li class="contact__element">
                    <img class="icon__twitter" width="24" height="20" alt="twitter" src="../images/twitter.svg">
                </li>
                <li class="contact__element">
                    <img class="icon__instagram" width="22" height="22" alt="instagram" src="../images/instagram.svg">
                </li>
                <li class="contact__element">
                    <img class="icon__pinterest" width="22" height="22" alt="pinterest" src="../images/pinterest.svg">
                </li>
            </ul>
        </div>
        <div class="footer__authorship">
            <p class="сopyright">
                © 2022 - Mariya Kobernik. All Rights Reserved. Designed & Developed by <span class="pixel"> Mariya Kobernik Team  </span> 
            </p>
        </div>
    </footer>
</body>
</html>