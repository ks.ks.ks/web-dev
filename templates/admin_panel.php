<?php
header('Content-type: text/html; charset=utf-8');
session_start();
if (! $_SESSION['admin'])
header('Location: adminavt.php');
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/avtadministrator.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,100&display=swap" rel="stylesheet">
    
    <title>Games Catalog. Административная панель</title>
</head>
<body>
    <header class="header">
        <div class = logo>
            <h2 class="header__title">Административная панель</h2>
        </div>   
    </header>
    <main class="main">
        <div class = "main__middle__admin">
            <form action = "create_games.php" method = "post">
                <button class ='submit_admin' type = "submit">Добавить игру</button>
            </form>
            <form action = "update.php" method = "post">
                <button class ='submit_admin' type = "submit">Редактировать игру</button>
            </form>
            <form action = "delete_game.php" method = "post">
                <button class ='submit_admin' type = "submit">Удалить игру</button>
            </form>
            <form action = "admin_logout.php" method = "post">
                <button class ='submit_admin' type="submit">Выйти</button>
            </form>
        </div>
    </main>
    <footer class="footer">
        <div class="footer__authorship">
            <p class="сopyright">
                © 2022 - Mariya Kobernik. All Rights Reserved. Designed & Developed by <span class="pixel"> Mariya Kobernik Team  </span> 
            </p>
        </div>
    </footer>

</body>
</html>