<?php
header('Content-type: text/html; charset=utf-8');
session_start();
if (! $_SESSION['admin'])
header('Location: adminavt.php');
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/avtadministrator.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,100&display=swap" rel="stylesheet">
    
    <title>Games Catalog. Добавление игр</title>
</head>
<body>
    <header class="header">
            <div class = logo>
                <h2 class="header__title">Добавление игр</h2>
                <h2 class="header__title">Список жанров</h2> 
            </div>   
        </header>

    <main class="main">
        <div class="main__middle">
            <form class = "form_avt" action="create_game.php" method="post">
                <input class = "form_avt" name="title_game" placeholder="Название игры">
                <input class = "form_avt" type="text" name="link_img" placeholder="Ссылка на картинку">
                <!--<input class = "form_avt" type="text" name="release_date" placeholder="Дата выхода">-->
                <input class = 'form_avt' type='date' name="release_date" value="Дата выхода" /></p>
                <input class = "form_avt" type="text" name="developer" placeholder="Разработчик">
                <input class = "form_avt" type="text" name="description" placeholder="Описание игры">
                <input class = "form_avt" type="text" name="number_genre" placeholder="Номер жанра">
                <input class = "form_avt_submit" type="submit" value="Добавить">
            </form>
            <?php
	            
	            if (isset($_SESSION['flash'])) {
		            echo $_SESSION['flash'];
		            unset($_SESSION['flash']);
	            }
            ?>
        </div>
        <div class = main__right>
        <?php
            $conn = new mysqli("localhost", "root", "", "games_db");
            if($conn->connect_error){
                die("Ошибка: " . $conn->connect_error);
            }
            $sql = "SELECT * FROM genres";
            if($result = $conn->query($sql)){

                echo "<table><tr><th>Номер жанра</th><th>Название</th></tr>";
                foreach($result as $row){
                    echo "<tr>";
                    echo "<td>" . $row["id_genre"] . "</td>";
                    echo "<td>" . $row["title"] . "</td>";
                    echo "</tr>";
                }
                echo "</table>";
        $result->free();
        } 
        else{
            echo "Ошибка: " . $conn->error;
        }
        $conn->close();
        ?>
                    
        </div>
    </main>
    <footer class="footer">
        <div class="footer__authorship">
            <p class="сopyright">
                © 2022 - Mariya Kobernik. All Rights Reserved. Designed & Developed by <span class="pixel"> Mariya Kobernik Team  </span> 
            </p>
        </div>
    </footer>
</body>
</html>
