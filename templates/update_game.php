<?php
header('Content-type: text/html; charset=utf-8');
session_start();
if (! $_SESSION['admin'])
header('Location: adminavt.php');
?>
<?php 
$conn = new mysqli("localhost", "root", "", "games_db");
if($conn->connect_error){
    die("Ошибка: " . $conn->connect_error);
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <title>Games Catalog. Изменения игр</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/avtadministrator.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,100&display=swap" rel="stylesheet">
</head>
<body>
    <main class = "main">
        
        <div class = "main__middle__up">
        <?php

        if($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET["id"]))
        {
            $title = $conn->real_escape_string($_GET["id"]);
            $sql = "SELECT title, developer, description, id_genre,image,release_date FROM game WHERE title = '$title'";
            if($result = $conn->query($sql)){
                if($result->num_rows > 0){
                    foreach($result as $row){
                        $gametitle = $row["title"];
                        $developer = $row["developer"];
                        $description = $row["description"];
                        $id_genre = $row["id_genre"];
                        $image = $row["image"];
                        $release_date = $row["release_date"];
                    }
                    echo "<h2 class = 'header__title'>Обновление игры</h2>
                        <form class = 'form__up' method='post'>
                            <input type='hidden' name='title' value='$title' />
                            <p>Название:
                            <input class = 'form_avt' type='text' name='new_title' value='$title' /></p>
                            <p>Разработчик:
                            <input class = 'form_avt' type='text' name='developer' value='$developer' /></p>
                            <p>Описание:
                            <input class = 'form_avt' type='text' name='description' value='$description' /></p>
                            <p>Жанр:
                            <input class = 'form_avt' type='number' name='id_genre' value='$id_genre' /></p>
                            <p>Изображение:
                            <input class = 'form_avt' type='text' name='image' value='$image' /></p>
                            <p>Дата выхода:
                            <input class = 'form_avt' type='date' name='release_date' value='$release_date' /></p>
                            <input class = 'form_avt_submit' type='submit' value='Сохранить'>
                    </form>";
            
                }
                else{
                    echo "<div>Игра не найдена</div>";
                }
                $result->free();
            } else{
                echo "Ошибка: " . $conn->error;
            }
        }
        elseif (isset($_POST["title"]) && isset($_POST["new_title"]) && isset($_POST["developer"]) && isset($_POST["description"]) && isset($_POST["id_genre"]) && isset($_POST["image"]) && isset($_POST["release_date"])) {
      
            $title = $conn->real_escape_string($_POST["title"]);
            $new_title = $conn->real_escape_string($_POST["new_title"]);
            $developer = $conn->real_escape_string($_POST["developer"]);
            $description = $conn->real_escape_string($_POST["description"]);
            $id_genre = $conn->real_escape_string($_POST["id_genre"]);
            $image = $conn->real_escape_string($_POST["image"]);
            $release_date = $conn->real_escape_string($_POST["release_date"]);
            $sql = "UPDATE game SET title = '$new_title', developer = '$developer', description = '$description', id_genre = '$id_genre', image = '$image', release_date = '$release_date' WHERE title = '$title'";
            if($result = $conn->query($sql)){
                header("Location: update.php");
            } else{
                echo "Ошибка: " . $conn->error;
            }
        }
        else{
            echo "Некорректные данные";
        }   
        $conn->close();
        ?>
        </div>
        <div class = "main__right__up">
        <?php
            $conn = new mysqli("localhost", "root", "", "games_db");
            if($conn->connect_error){
                die("Ошибка: " . $conn->connect_error);
            }
            echo "<h2 class = 'header__title'>Список жанров</h2>";
            $sql1 = "SELECT * FROM genres";
            if($result1 = $conn->query($sql1)){

                echo "<table><tr><th>Номер жанра</th><th>Название</th></tr>";
                foreach($result1 as $row1){
                    echo "<tr>";
                    echo "<td>" . $row1["id_genre"] . "</td>";
                    echo "<td>" . $row1["title"] . "</td>";
                    echo "</tr>";
                }
                echo "</table>";
        $result1->free();
        } 
        else{
            echo "Ошибка: " . $conn->error;
        }
        $conn->close();
        ?>
</body>
</html>