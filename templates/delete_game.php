<?php
header('Content-type: text/html; charset=utf-8');
session_start();
if (! $_SESSION['admin'])
header('Location: adminavt.php');
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <title>Games Catalog. Удаление игр</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/avtadministrator.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,100&display=swap" rel="stylesheet">
</head>
<body>
    <header class="header">
        <div class = logo>
            <h2 class="header__title">Список игр</h2>
        </div>   
    </header>
    <main class = "main__update">
    
        <?php
            $conn = new mysqli("localhost", "root", "", "games_db");
            if($conn->connect_error){
            die("Ошибка: " . $conn->connect_error);
        }
            $sql = "SELECT title FROM game";

            if($result = $conn->query($sql)){
                echo "<table class = 'update_table'><tr><th></th></tr>";
                foreach($result as $row){
                    echo "<tr>";
                    
                    echo "<td><form action='delete_games.php' method='post'>
                                <input type='hidden' name='title' value='" . $row["title"] . "' />
                                <input type='text' name='title1' value='" . $row["title"] . "' />
                                <input type='submit' value='Удалить'></form></td>";
                    echo "</tr>";
                }
            echo "</table>";

            $result->free();

            } else{
                echo "Ошибка: " . $conn->error;
            }
            $conn->close();
?>
    </main>
</body>
</html>
