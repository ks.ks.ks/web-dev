<!DOCTYPE html>
<html lang="en">
<head>
<!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();
        for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
            k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
 
        ym(93760794, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
 <noscript><div><img src="https://mc.yandex.ru/watch/93760794" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
 <!-- /Yandex.Metrika counter -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/catalog.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,100&display=swap" rel="stylesheet">

    <title>Games Catalog</title>
</head>
<body>
<header class="header">
        <div class = logo>
            <h2 class="header__title">Games Catalog</h2>
            <img class = "img_logo" width="125" height="125" alt="Gaming" src="../images/LOGO_PNG.png">
        </div>
        <ul class="header__navigation">
            <li class="header__element">
                <h3><a class="nav__element" href="../index.php">Домой</a></h3>
            </li>
            <li class="header__element">
                <div class="dropdown">
                    <h3 class="nav__element">Категории🔽</h3>
                    <div class="dropdown-content">
                      <a href="catalog_genres.php">Жанры</a>
                      <a href="catalog_themes.php">Темы</a>
                      <a href="catalog_number_of_players.php">Количество игроков</a>
                    </div>
                  </div>
            </li>
            <li class="header__element">
                <h3 class="nav__element">О нас</h3>
            </li>
            <li class="header__element">
                <h3 class="nav__element">Контакты</h3>
            </li>
        </ul>    
    </header>
    <main class="main">
        <div class="main__middle">
            <div class="main__middle--text">
                <p class="genres_name">
                    <a href="game_template.php?id_genre=7"> Выживание</a>
                </p>
                <p class="genres_name">
                    <a href="game_template.php?id_genre=8"> Космос</a>
                </p>
                <p class="genres_name">
                    <a href="game_template.php?id_genre=9"> Открытый мир</a>
                </p>
                <p class="genres_name">
                    <a href="game_template.php?id_genre=10"> Тайны и детективы</a>
                </p>
                <p class="genres_name">
                    <a href="game_template.php?id_genre=11"> Хоррор</a>
                </p>
                <p class="genres_name">
                    <a href="game_template.php?id_genre=12"> Научная фантастика</a> 
                </p>
                <p class="genres_name">
                    <a href="game_template.php?id_genre=13"> Киберпанк</a> 
                </p>
                
            </div>
            <div class="category">
                <p class = "categ">
                    Темы
                </p>
            </div>
            <img class="main__middle--img" width="500" height="350" alt="The mountains" src="https://fikiwiki.com/uploads/posts/2022-02/1644940754_1-fikiwiki-com-p-kartinki-far-krai-5-1.jpg">
        </div>
    </main>
    <?php require 'footer.html' ?>
