<?php
header('Content-type: text/html; charset=utf-8');
session_start();
if (! $_SESSION['admin'])
header('Location: adminavt.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/web-dev/styles/avtadministrator.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@0;1&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,100&display=swap" rel="stylesheet">
    
    <title>Games Catalog. Редактирование игр</title>
</head>
<body>
    <header class="header">
            <div class = head>
                <h2 class="header__title">Редактирование игр</h2>
                <h5 class="header__title">Список игр</h5> 
            </div>   
        </header>

    <main class="main">  
        <div class = main__middle>
        <form class = "form_avt" action="update_game.php" method="post">
            <?php
                $conn = new mysqli("localhost", "root", "", "games_db");
                if($conn->connect_error){
                    die("Ошибка: " . $conn->connect_error);
                }
                $sql = "SELECT title FROM game";
                if($result = $conn->query($sql)){
                    foreach($result as $row){?>
                        <div class = titl_game>
                        <?php
                        echo "<tr>";
                        echo "<td>" . $row["title"] . "</td>";
                        echo "</tr>";?>
                        </div>
                        <?php
                    }
            
            $result->free();
            } 
            else{
                echo "Ошибка: " . $conn->error;
            }
            $conn->close();
            ?>
            <input class = "form_avt_submit" type="submit" value="Добавить">
        </form>     
        </div>
    </main>
    <footer class="footer">
        <div class="footer__authorship">
            <p class="сopyright">
                © 2022 - Mariya Kobernik. All Rights Reserved. Designed & Developed by <span class="pixel"> Mariya Kobernik Team  </span> 
            </p>
        </div>
    </footer>
</body>
</html>
